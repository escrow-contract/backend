import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";
import { ethers } from "ethers";

@ValidatorConstraint({ name: "IsPrivateKey", async: false })
class IsPrivateKeyConstraint implements ValidatorConstraintInterface {
  validate(privateKey: string, args: ValidationArguments) {
    try {
      return !!new ethers.Wallet(privateKey);
    } catch (error) {
      return false;
    }
  }

  defaultMessage(args: ValidationArguments) {
    return "Private key ($value) is not a valid Ethereum private key!";
  }
}

export function IsPrivateKey(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsPrivateKeyConstraint,
    });
  };
}
