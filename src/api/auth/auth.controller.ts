import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import { hasher } from "../../helpers/hasher.helper";

interface LocalUser extends Express.User {
  id: number;
  username: string;
}

export const signIn = (req: Request, res: Response) => {
  const user = req.user as LocalUser | undefined;
  if (user && user.id) {
    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET_KEY as string, { expiresIn: "1h" });
    res.json({ token });
  } else {
    res.status(401).send("email or password incorrect!!");
  }
};

export const auth = (req: Request, res: Response) => {
  const dataString = JSON.stringify(req.body);
  const hash = hasher.hashWithData(dataString, "sha3-256");
  if (hash === "74fe56b1739b58a9faecb3a967d2847f011998b3b132ab24a8d1e14581d331e8") {
    res.send("ok");
  } else {
    res.send("error");
  }
};
