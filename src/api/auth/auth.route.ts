import { Router } from "express";
import { auth, signIn } from "./auth.controller";
import { pp } from "../../middleware/auth";

const router = Router();

router.post("/sign-in", pp.localGuard, signIn);
router.get("/sign-in", pp.jwtGuard, auth);

export default router;
