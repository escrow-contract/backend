import { NextFunction, RequestHandler, Router, Request, Response } from "express";
import { createUser, deposit, getUser, transactions, withdraw } from "./blockchain.controller";

const router = Router();

const asyncHandler = (fn: (req: Request, res: Response, next: NextFunction) => Promise<any>): RequestHandler => {
  return (req, res, next) => {
    Promise.resolve(fn(req, res, next)).catch(next);
  };
};

router.post("/create-user", asyncHandler(createUser));
router.post("/transactions", asyncHandler(transactions));
router.post("/deposit", deposit);
router.post("/withdraw", withdraw);
router.get("/get-user", getUser);

export default router;
