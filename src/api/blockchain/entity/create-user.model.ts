import { IsBoolean, IsIn, IsNotEmpty, IsNumber } from "class-validator";
import { IsValidChecksum } from "../../../decorators/is-valid-checksum.decorator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";
import { IsPrivateKey } from "../../../decorators/is-private-key.decorator";
import { User } from "@prisma/client";

export class CreateUserEntity {
  @IsNotEmpty()
  @IsEthereumAddress()
  wallet!: string;

  @IsNotEmpty()
  @IsPrivateKey()
  privateKey!: string;

  @IsBoolean()
  isMerchant!: boolean;

  @IsNotEmpty()
  @IsIn(["bsc:testnet", "trc:testnet"])
  network!: string;

  @IsNotEmpty()
  @IsValidChecksum(["wallet", "privateKey", "isMerchant", "network"])
  checksum!: string;
}
