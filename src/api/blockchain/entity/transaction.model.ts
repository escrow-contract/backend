import { IsIn, IsNotEmpty, IsString } from "class-validator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";

export class TransactionEntity {
  @IsString()
  @IsNotEmpty()
  @IsEthereumAddress()
  wallet!: string;

  @IsString()
  @IsNotEmpty()
  @IsIn(["bsc:testnet", "tron:testnet"])
  network!: string;
}
