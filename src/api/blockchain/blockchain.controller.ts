import { ethers, Result } from "ethers";
import { NextFunction, Request, Response } from "express";
import { NetworkRoute } from "../../blockchain/network/network.interface";
import { Token__factory } from "../../blockchain/typechain-types";
import { prisma } from "../../db/database.service";
import { hasher } from "../../helpers/hasher.helper";
import { BscTestnetNetwork } from "../../blockchain/network/bsc.network";
import { transformer } from "../../helpers/transformer.helper";
import { CreateUserEntity } from "./entity/create-user.model";
import { Conflict, BadRequest, InternalServerError } from "http-errors";
import { DateTime } from "luxon";
import { TransactionEntity } from "./entity/transaction.model";

export interface TransactionResult {
  status: string;
  message: string;
  result: Transaction[];
}

export interface Transaction {
  blockNumber: string;
  blockHash: string;
  timeStamp: string;
  hash: string;
  nonce: string;
  transactionIndex: string;
  from: string;
  to: string;
  value: string;
  gas: string;
  gasPrice: string;
  input: string;
  methodId: string;
  functionName: string;
  contractAddress: string;
  cumulativeGasUsed: string;
  txreceipt_status: string;
  gasUsed: string;
  confirmations: string;
  isError: string;
}

export const createUser = async (req: Request, res: Response, next: NextFunction) => {
  const body = transformer.validateRequestBody<CreateUserEntity>(CreateUserEntity, req.body);

  const { wallet, network, isMerchant, privateKey } = body;
  const user = await prisma.user.findUnique({ where: { wallet } });
  if (user) throw new Conflict("User already exists");

  const _network = NetworkRoute.getNetwork(network);
  const txReceipt = await _network.createUser(wallet, isMerchant);

  if (!txReceipt || txReceipt.status === 0) throw new BadRequest("Create user failed");

  const userCreate = await prisma.user.create({
    data: {
      wallet,
      network,
      privateKey,
      isMerchant,
    },
  });
  res.status(200).send({
    txReceipt,
    user: {
      id: userCreate.id,
      wallet: userCreate.wallet,
      network: userCreate.network,
      isMerchant: userCreate.isMerchant,
      status: "success",
    },
  });
};

export const transactions = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<TransactionEntity>(TransactionEntity, req.body);

  console.log(123);

  const network = NetworkRoute.getNetwork(body.network);
  const iface = Token__factory.createInterface();
  const selector = iface.getFunction("transferFrom").selector;

  let transactions = await network.getTransactionByTo(body.wallet);
  console.log(transactions);

  transactions = transactions.filter((t) => {
    const currentTime = parseInt(t.timeStamp);
    const endTime = DateTime.now().toSeconds();
    const startTime = DateTime.now().minus({ minutes: 15 }).toSeconds();
    if (startTime > currentTime || currentTime > endTime) return false;
    return t.methodId === selector;
  });

  if (!transactions.length) {
    res.status(200).send([]);
  }

  let data: any[] = [];
  for (let index = 0; index < transactions.length; index++) {
    const { input, timeStamp, from, to, value, hash } = transactions[index];
    const checksum = hasher.getCheckSum({ input, timeStamp, from, to, value, hash, network });
    data = [...data, { input, timeStamp, from, to, value, hash, checksum }];
  }
  res.status(200).send(data);
};

export const deposit = async (req: Request, res: Response) => {
  try {
    const { input, hash, network } = req.body;
    const functionFragment = ethers.FunctionFragment.from(input);
    const decodeArgs: Result = Token__factory.createInterface().decodeFunctionData(functionFragment, input);

    const wallet: string = decodeArgs[0];
    const amount: bigint = decodeArgs[1];

    if (!ethers.isAddress(wallet)) {
      return res.status(400).send("deposit failed: invalid wallet address");
    }
    const user = await prisma.user.findUnique({ where: { wallet } });
    if (!user) {
      return res.status(400).send("deposit failed: user not found");
    }

    const _network = NetworkRoute.getNetwork(network);
    const txReceipt = await _network.deposit(wallet, amount.toString());

    if (txReceipt && txReceipt.status === 1) {
      const txEntry = await prisma.transaction.create({
        data: {
          to: wallet,
          amount: amount,
          hash: txReceipt.hash,
          network,
          status: true,
        },
      });

      return res.status(200).send({ txReceipt, txEntry });
    } else {
      return res.status(400).send("deposit failed: " + JSON.stringify(txReceipt));
    }
  } catch (err) {
    return res.status(400).send("deposit failed: " + (err as any));
  }
};

export const withdraw = async (req: Request, res: Response) => {
  try {
    const { wallet, amount, network, checksum } = req.body;
    const user = await prisma.user.findUnique({ where: { wallet } });
    if (!user) {
      return res.status(400).send("withdraw failed: user not found");
    }
    if (!wallet || !amount || !network) {
      return res.status(400).send("withdraw failed: missing parameters");
    }
    if (network !== "bsc:testnet" && network !== "tron:testnet") {
      return res.status(400).send("withdraw failed: accept BSC or TRC network");
    }
    if (hasher.getCheckSum({ address: wallet, amount, network }) !== checksum) {
      return res.status(400).send("withdraw failed: checksum invalid");
    }
    if (!ethers.isAddress(wallet)) {
      return res.status(400).send("withdraw failed: invalid wallet address");
    }
    if (BigInt(amount) <= 0) {
      return res.status(400).send("withdraw failed: invalid amount");
    }

    const _network = NetworkRoute.getNetwork(network);
    const txReceipt = _network.withdraw(wallet, amount);
    return res.status(200).send({ txReceipt });
  } catch (err) {
    return res.status(400).send("withdraw failed: " + (err as any));
  }
};

export const getUser = async (req: Request, res: Response) => {
  try {
    const { wallet, network } = req.body;
    const _network = NetworkRoute.getNetwork(network);
    const user = await (_network as BscTestnetNetwork).getUser(wallet);
    return res.status(200).send({
      isMerchant: user.isMerchant,
      staked: user.staked.toString(),
      balance: user.balance.toString(),
      timeStamp: user.timestamp.toString(),
    });
  } catch (err) {
    return res.status(400).send("user not found: " + (err as any));
  }
};
