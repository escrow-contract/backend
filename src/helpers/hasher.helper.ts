import { createHash } from "crypto";
import { compareSync, genSaltSync, hashSync } from "bcryptjs";
import crypto from "crypto-js";

const hash = (plaintext: string) => hashSync(plaintext, genSaltSync());

const compare = (plaintext: string, hash: string) => compareSync(plaintext, hash);

const randomHash = () => hashWithData(randomStringGenerator(32));

const hashWithData = (data: string, algorithm = "sha256"): string => createHash(algorithm).update(data).digest("hex");

const getCheckSum = (data: any) => crypto.SHA256(JSON.stringify(data)).toString(crypto.enc.Hex);

const randomStringGenerator = (length = 32, characters?: string) => {
  const _characters = characters || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let result = "";
  const charactersLength = _characters.length;
  for (let i = 0; i < length; i++) {
    result += _characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const hasher = {
  hash,
  compare,
  randomHash,
  hashWithData,
  randomStringGenerator,
  getCheckSum,
};
