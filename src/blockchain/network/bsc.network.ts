import { ContractTransactionReceipt, ethers } from "ethers";
import { Token, EscrowV2 } from "../typechain-types";
import { ContractHandler, INetwork } from "./network.interface";
import { bscEscrow, bscEscrowAddress, bscToken, getBscWallet, myBscWallet } from "..";
import axios from "axios";
import { transformer } from "../../helpers/transformer.helper";
import { TransactionResult } from "../../api/blockchain/entity/deposit.model";

export class BscTestnetNetwork implements INetwork {
  token: ContractHandler<Token>;
  escrow: ContractHandler<EscrowV2>;

  constructor(token: ContractHandler<Token>, contract: ContractHandler<EscrowV2>) {
    this.token = token;
    this.escrow = contract;
  }

  async createUser(address: string, isMerchant: boolean): Promise<ContractTransactionReceipt | null> {
    const createUserTx = await bscEscrow.connect(myBscWallet).createUser(address, isMerchant);
    return await createUserTx.wait();
  }

  async getOwner(): Promise<string> {
    return await bscEscrow.connect(myBscWallet).getOwner();
  }

  async deposit(privateKey: string, amount: string): Promise<ContractTransactionReceipt | null> {
    const wallet = getBscWallet(privateKey);
    const allowance = await bscToken.connect(wallet).allowance(wallet.address, bscEscrowAddress);

    if (allowance < BigInt(amount)) {
      const approveTx = await bscToken.connect(wallet).approve(bscEscrowAddress, ethers.MaxUint256);
      const approveReceipt = await approveTx.wait();
      if (!approveReceipt || approveReceipt.status !== 1) return null;
    }

    const depositTx = await bscEscrow.connect(wallet).deposit(amount);
    return await depositTx.wait();
  }

  async withdraw(privateKey: string, amount: number) {
    const wallet = getBscWallet(privateKey);
    const withdrawTx = await bscEscrow.connect(wallet).withdraw(amount);
    return await withdrawTx.wait();
  }

  async getUser(address: string) {
    return await bscEscrow.connect(myBscWallet).getUser(address);
  }

  async getTransactions() {
    const response = await axios.get<TransactionResult>("https://api-testnet.bscscan.com/api", {
      params: {
        module: "account",
        action: "txlist",
        address: "0x89374703d460608d749B78429b34C8134BD844ff",
        startblock: "0",
        endblock: "99999999",
        apikey: "V3GAX2161S42Y8FWFFQ4PB77WAH7924X52",
      },
    });

    return transformer.validateRequestBody<TransactionResult>(TransactionResult, response.data);
  }

  async getTransactionByTo(address: string) {
    const transactions = await this.getTransactions();
    if (transactions.status === "1") {
      return transactions.result.filter((tx) => tx.to === address);
    } else {
      return [];
    }
  }

  async getTransactionByFrom(address: string) {
    const transactions = await this.getTransactions();
    if (transactions.status === "1") {
      return transactions.result.filter((tx) => tx.from === address);
    } else {
      return [];
    }
  }

  async getTransactionByHash(hash: string) {
    const response = await this.getTransactions();
    if (response.status === "1") {
      return response.result.filter((tx) => tx.hash === hash);
    } else {
      return [];
    }
  }
}
