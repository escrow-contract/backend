import { Contract, ContractTransactionReceipt, Interface, JsonRpcProvider, Wallet } from "ethers";
import { EscrowV2, EscrowV2__factory, Token, Token__factory } from "../typechain-types";
import { BscTestnetNetwork } from "./bsc.network";
import { Transaction, TransactionResult } from "../../api/blockchain/blockchain.controller";
// import { TronNetwork } from "./tron.network";

export interface INetworkVariables {
  token: ContractHandler<Token>;
  escrow: ContractHandler<EscrowV2>;
}

export interface INetwork extends INetworkVariables {
  createUser(address: string, isMerchant: boolean): Promise<ContractTransactionReceipt | null>;
  deposit(address: string, amount: string): Promise<ContractTransactionReceipt | null>;
  withdraw(address: string, amount: number): Promise<ContractTransactionReceipt | null>;
  getTransactions(): Promise<TransactionResult>;
  getTransactionByTo(address: string): Promise<Transaction[]>;
  getTransactionByFrom(address: string): Promise<Transaction[]>;
}

const privateKey = process.env.PRIVATE_KEY as string;

// BSC
const bscTestnetRpc = process.env.BSC_TESTNET_RPC as string;
const bscTokenAddress = process.env.BSC_TOKEN_ADDRESS as string;
const bscEscrowAddress = process.env.BSC_ESCROW_ADDRESS as string;

// TRON
// const tronRpc = process.env.TRON_RPC as string;
// const tronTokenAddress = process.env.TRON_TOKEN_ADDRESS as string;
// const tronEscrowAddress = process.env.TRON_ESCROW_ADDRESS as string;

export class ContractHandler<T> {
  provider = new JsonRpcProvider(this.rpc);
  wallet = new Wallet(this.privateKey, this.provider);
  contract = new Contract(this.address, this.abi, this.wallet) as T;

  constructor(
    readonly rpc: string,
    readonly address: string,
    readonly abi: Interface,
    readonly privateKey: string,
  ) {}
}

const tokenOnBSC = new ContractHandler<Token>(
  bscTestnetRpc,
  bscTokenAddress,
  Token__factory.createInterface(),
  privateKey,
);

const escrowOnBSC = new ContractHandler<EscrowV2>(
  bscTestnetRpc,
  bscEscrowAddress,
  EscrowV2__factory.createInterface(),
  privateKey,
);

// const tokenOnTron = new IContract<Token>(tronRpc, tronTokenAddress, Token__factory.createInterface(), privateKey);
// const escrowOnTron = new IContract<EscrowV2>(
//   tronRpc,
//   tronEscrowAddress,
//   EscrowV2__factory.createInterface(),
//   privateKey,
// );

export class NetworkFactory {
  static createNetWork(
    networkType: string,
    token: ContractHandler<Token>,
    contract: ContractHandler<EscrowV2>,
  ): INetwork {
    switch (networkType) {
      case "bsc:testnet":
        return new BscTestnetNetwork(token, contract);
      case "tron:testnet":
      // return new TronNetwork(token, contract);
      default:
        throw new Error("Network not found");
    }
  }
}

export const bscNetwork = NetworkFactory.createNetWork("bsc:testnet", tokenOnBSC, escrowOnBSC);

export class NetworkRoute {
  static getNetwork = (networkType: string) => {
    switch (networkType) {
      case "bsc:testnet":
        return bscNetwork;
      default:
        throw new Error("Network not found");
    }
  };
}
