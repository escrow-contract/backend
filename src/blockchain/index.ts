import { Contract, JsonRpcProvider, Provider, Wallet } from "ethers";
import { EscrowV2, EscrowV2__factory, Token, Token__factory } from "./typechain-types";

export const privateKey = process.env.PRIVATE_KEY as string;

export const bscTestnetRpc = process.env.BSC_TESTNET_RPC as string;
export const bscTokenAddress = process.env.BSC_TOKEN_ADDRESS as string;
export const bscEscrowAddress = process.env.BSC_ESCROW_ADDRESS as string;

export const bscProvider = new JsonRpcProvider(bscTestnetRpc as string);

export const bscTokenInterface = Token__factory.createInterface();
export const bscEscrowInterface = EscrowV2__factory.createInterface();

export const bscToken = new Contract(bscTokenAddress, bscTokenInterface) as unknown as Token;
export const bscEscrow = new Contract(bscEscrowAddress, bscEscrowInterface) as unknown as EscrowV2;

const _getWallet = (address: string, provider: Provider) => new Wallet(address, provider);
export const getBscWallet = (address: string) => _getWallet(address, bscProvider);
export const myBscWallet = getBscWallet(privateKey);
