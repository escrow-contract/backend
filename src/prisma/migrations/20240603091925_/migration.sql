/*
  Warnings:

  - You are about to drop the column `private` on the `User` table. All the data in the column will be lost.
  - Added the required column `privateKey` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "address" TEXT NOT NULL,
    "privateKey" TEXT NOT NULL,
    "isMerchant" BOOLEAN NOT NULL
);
INSERT INTO "new_User" ("address", "id", "isMerchant") SELECT "address", "id", "isMerchant" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
PRAGMA foreign_key_check("User");
PRAGMA foreign_keys=ON;
