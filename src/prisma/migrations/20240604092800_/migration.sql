-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Transaction" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "hash" TEXT NOT NULL,
    "status" BOOLEAN NOT NULL,
    "to" TEXT NOT NULL,
    "amount" BIGINT NOT NULL,
    "network" TEXT NOT NULL
);
INSERT INTO "new_Transaction" ("amount", "hash", "id", "network", "status", "to") SELECT "amount", "hash", "id", "network", "status", "to" FROM "Transaction";
DROP TABLE "Transaction";
ALTER TABLE "new_Transaction" RENAME TO "Transaction";
CREATE UNIQUE INDEX "Transaction_hash_key" ON "Transaction"("hash");
PRAGMA foreign_key_check("Transaction");
PRAGMA foreign_keys=ON;
