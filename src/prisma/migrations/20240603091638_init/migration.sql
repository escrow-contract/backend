-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "address" TEXT NOT NULL,
    "private" TEXT NOT NULL,
    "isMerchant" BOOLEAN NOT NULL
);
