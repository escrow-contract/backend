-- CreateTable
CREATE TABLE "Transaction" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "hash" TEXT NOT NULL,
    "status" BOOLEAN NOT NULL,
    "to" BIGINT NOT NULL,
    "amount" BIGINT NOT NULL,
    "network" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Transaction_hash_key" ON "Transaction"("hash");
