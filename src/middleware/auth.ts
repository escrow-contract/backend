import passport, { initialize } from "passport";
import { Strategy as JwtStrategy, ExtractJwt, StrategyOptionsWithSecret } from "passport-jwt";

import { IStrategyOptions, Strategy as LocalStrategy } from "passport-local";

interface JwtPayload {
  id: string;
  username: string;
}

class PassportMiddleware {
  public jwtGuard = passport.authenticate("jwt", { session: false });
  public localGuard = passport.authenticate("local", {
    session: false,
  });

  constructor() {
    this.initJwtPassport();
    this.localPassport();
  }

  private initJwtPassport() {
    const config: StrategyOptionsWithSecret = {
      secretOrKey: process.env.JWT_SECRET_KEY as string,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    };

    passport.use(
      new JwtStrategy(config, (payload: JwtPayload, done) => (payload.id ? done(null, payload) : done(null, false))),
    );
  }

  private localPassport() {
    const config: IStrategyOptions = {
      usernameField: "email",
      passwordField: "password",
    };
    passport.use(
      new LocalStrategy(config, (email, password, done) => {
        if (email === "dev.anhtu@gmail.com" && password === "123123123") {
          return done(null, { id: 4, email: email });
        }
        return done(null, {});
      }),
    );
  }

  initialize() {
    return passport.initialize();
  }
}

export const pp = new PassportMiddleware();
