require("dotenv").config();
import express, { Application, Response, Request, NextFunction } from "express";
import { pp } from "./middleware/auth";
import authRoutes from "./api/auth/auth.route";
import blockchainRoutes from "./api/blockchain/blockchain.route";
import cors from "cors";

const app: Application = express();

app.use(cors());
app.use(express.json());
app.use(pp.initialize());

// route
app.use("/auth", authRoutes);
app.use("/blockchain", blockchainRoutes);

app.use((err: Error, req: Request, res: Response, next: NextFunction): void => {
  const code = (err as any)?.status || 500;
  res.status((err as any)?.status || 500).json({ status: code, error: err.message });
});

// listen
app.listen(process.env.PORT, () => {
  console.log(`Server running on port ${process.env.PORT}`);
});

export default app;
